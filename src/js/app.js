const menuBtn = document.querySelector(".header__burger");
const menuShow = document.querySelector('.nav-burger');
const language = document.querySelector('.language');
const languageShow = document.querySelector('.language__hiden');


menuBtn.addEventListener("click", (e) => {
    const target = e.target.closest("button");
    for (const i of target.children) {
        i.classList.toggle("header__burger--hidden");
    }
    menuShow.classList.toggle('menuShow');
})



